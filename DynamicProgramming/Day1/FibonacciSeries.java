package Day1;

/**
 * https://leetcode.com/problems/fibonacci-number/
 */

public class FibonacciSeries {

    public static int fib(int n) {

        if (n < 2)
            return n;
        int[] dp = new int[n+1];
        dp[0] = 0;
        dp[1] = 1;

        for (int i=2; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }

    public static void main(String[] a) {
        System.out.println(fib(6));
    }
}

package Day1;

/**
 * https://leetcode.com/problems/min-cost-climbing-stairs/
 */
public class MinCostClimbingStairs {

    public static int minCostClimbingStairs(int[] cost) {
       int[] minCost = new int[cost.length + 1];

       for (int i=2; i < minCost.length; i++) {
        int takeOneStep = minCost[i-1] + cost[i-1];
        int takeTwoStep = minCost[i-2] + cost[i-2];
        minCost[i] = Math.min(takeOneStep, takeTwoStep);
       }
       return minCost[minCost.length - 1];
    }

    public static void main(String[] a) {
       int[] cost = {10,15,20};
       System.out.println(minCostClimbingStairs(cost));
    }
}

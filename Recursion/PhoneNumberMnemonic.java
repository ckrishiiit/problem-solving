package Recursion;
import java.util.*;

//Time & Space O(4^n * n)
public class PhoneNumberMnemonic {

    static Map<Character, String[]> map = new HashMap<>();
    static {
        map.put('0', new String[]{"0"});
        map.put('1', new String[]{"1"});
        map.put('2', new String[]{"a","b","c"});
        map.put('3', new String[]{"d","e","f"});
        map.put('4', new String[]{"g","h","i"});
        map.put('5', new String[]{"j","k","l"});
        map.put('6', new String[]{"m","n","o"});
        map.put('7', new String[]{"p","q","r","s"});
        map.put('8', new String[]{"t","u","v"});
        map.put('9', new String[]{"w","x","y","z"});
    }
    public static ArrayList<String> phoneNumberMnemonics(String phoneNumber) {

        String[] current = new String[phoneNumber.length()];
        Arrays.fill(current, "0");

        ArrayList<String> res = new ArrayList<>();

        helper(0,phoneNumber, current, res);

        return res;
    }

    private static void helper(int idx, String phoneNumber, String[] current, ArrayList<String> res) {

        if (idx == phoneNumber.length()) {

            String mnemonic = String.join("", current);
            res.add(mnemonic);

        } else {
            char ch = phoneNumber.charAt(idx);
            String[] letters = map.get(ch);
            for (String letter: letters) {
                current[idx] = letter;
                helper(idx+1, phoneNumber, current, res);
            }
        }
    }

    public static void main(String args[]) {
        String str = "1905";
        ArrayList<String> mnemonics = phoneNumberMnemonics(str);
        for (String mnemonic : mnemonics) {
            System.out.println(mnemonic);
        }
    }
}

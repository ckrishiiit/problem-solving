package Recursion;
import java.util.*;

//Time - O(n) Space O(n)

public class StairCaseTraversal {

    public static int staircaseTraversal(int height, int maxSteps) {
        List<Integer> waysToTop = new ArrayList<Integer>();
        waysToTop.add(1);
        int totalSteps = 0;
        for (int currentHeight = 1; currentHeight <= height; currentHeight++) {

            int endOfWindow = currentHeight - 1;
            int startOfWindow = currentHeight - maxSteps - 1;

            if (startOfWindow >= 0) {
                totalSteps -= waysToTop.get(startOfWindow);
            }
            totalSteps += waysToTop.get(endOfWindow);
            waysToTop.add(totalSteps);
        }
        return totalSteps;
    }

    public static void main(String[] args) {
        System.out.println(staircaseTraversal(4,2));
    }
}

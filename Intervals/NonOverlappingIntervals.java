package Intervals;
import java.util.Arrays;
/**
 * https://leetcode.com/problems/non-overlapping-intervals/
 * Time - O(n log n)
 * Space - O(1)
 */
public class NonOverlappingIntervals {

    public int eraseOverlapIntervals(int[][] intervals) {

        // sort the intervals based on start time
        Arrays.sort(intervals, (a,b) -> a[0] - b[0]);
        int minCount = 0;
        int prevEnd = intervals[0][1];
        for (int i=1; i < intervals.length; i++) {
            int currentIntervalStart = intervals[i][0];
            if (currentIntervalStart < prevEnd) {
                minCount++;
                prevEnd = Math.min(prevEnd, intervals[i][1]);
            }else{
                prevEnd = Math.max(prevEnd,intervals[i][1] );
            }
        }
        return minCount;
    }
}

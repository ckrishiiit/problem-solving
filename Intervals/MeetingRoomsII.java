package Intervals;
import java.util.*;
/**
 * https://leetcode.com/problems/meeting-rooms-ii/
 * Time - O(N log N)
 * Space -- O(N)
 */
public class MeetingRoomsII {

    public int minMeetingRooms(int[][] intervals) {
        if (intervals == null || intervals.length == 0)
            return 0;

        if (intervals.length == 1)
            return 1;

        // sort the intervals based on start time
        Arrays.sort(intervals, (a,b) -> a[0]-b[0]);

        PriorityQueue<Integer> pq = new PriorityQueue<>( (a,b) -> Integer.compare(a,b));
        pq.add(intervals[0][1]);

        for (int i=1; i < intervals.length; i++) {
            int[] currentInterval = intervals[i];

            if (pq.peek() <= currentInterval[0])
                pq.poll();

            pq.add(currentInterval[1]);
        }
        return pq.size();
    }

    public static void main(String[] a) {
        int[][] intervals = { {0,30}, {5,10}, {15,20}};
        MeetingRoomsII obj = new MeetingRoomsII();
        int roomsRequired = obj.minMeetingRooms(intervals);
        System.out.println(roomsRequired);
    }
}

package Intervals;
import java.util.*;

/**
 * https://leetcode.com/problems/meeting-rooms/
 * Time : O(N log N)
 * space : O(1)
 */
public class MeetingRooms {

    public boolean canAttendMeetings(int[][] intervals) {

        if (intervals == null || intervals.length <= 1)
            return true;

        //sort the intervals
        Arrays.sort(intervals, (a,b) -> a[0] - b[0]);

        // if any overlap interval exists then return true..
        List<int[]> meetings = new ArrayList<>();
        meetings.add(intervals[0]);

        for (int i=1;i < intervals.length; i++) {
            int[] currentInterval = intervals[i];
            int[] previousInterval = meetings.get(meetings.size()-1);

            if (previousInterval[1] > currentInterval[0])
                return false;
            else
                meetings.add(currentInterval);
        }
        return true;
    }
}

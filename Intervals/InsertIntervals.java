package Intervals;
import java.util.*;

/**
 *  https://leetcode.com/problems/insert-interval/
 *  Time : O(n)
 *  Space : O(n)
 */
public class InsertIntervals {

    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> res = new ArrayList<>();

        for (int[] interval : intervals) {

            int[] currentInterval = interval;
            if (newInterval[1] < currentInterval[0]) {
                res.add(newInterval);
                newInterval = currentInterval;
            }
            else if (currentInterval[1] < newInterval[0]){
                res.add(currentInterval);
            }else {
                newInterval[0] = Math.min(newInterval[0], currentInterval[0]);
                newInterval[1] = Math.max(newInterval[1], currentInterval[1]);
            }
        }
        res.add(newInterval);
        return res.toArray(new int[res.size()][]);
    }
}

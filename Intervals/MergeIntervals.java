package Intervals;
import java.util.*;

/**
 * https://leetcode.com/problems/merge-intervals/
 * Time O(N log N)
 * Space O(1) -- not including o/p array
 */
public class MergeIntervals {

    public int[][] merge(int[][] intervals) {
        if (intervals == null || intervals.length <= 1)
            return intervals;

        //sort the intervals based on start time
        Arrays.sort(intervals, (a,b) -> a[0] - b[0]);

        List<int[]> res = new ArrayList<>();
        res.add(intervals[0]);
        for (int[] interval : intervals) {
            int[] previousInterval = res.get(res.size()-1);
            int[] currentInterval = interval;

            //merge when previousInterval end >= currentInterval start
            if (previousInterval[1] >= currentInterval[0]) {
                previousInterval[1] = Math.max(previousInterval[1],currentInterval[1]);
            }else{
                res.add(currentInterval);
            }
        }
        return res.toArray(new int[res.size()][]);
    }
}

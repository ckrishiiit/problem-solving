package Intervals;
import java.util.List;
import java.util.ArrayList;

/**
 * https://leetcode.com/problems/interval-list-intersections/
 * Time O(N)
 * Space O(1)
 */
public class IntervalListIntersection {

    public int[][] intervalIntersection(int[][] A, int[][] B) {

        List<int[]> res = new ArrayList<>();
        int lo = 0, hi = 0;
        int i=0, j = 0;

        while ( i < A.length && j < B.length) {

            lo = Math.max(A[i][0], B[j][0]);
            hi = Math.min(A[i][1], B[j][1]);

            if (lo <= hi)
                res.add(new int[]{lo, hi});

            if (A[i][1] < B[j][1])
                i++;
            else
                j++;
        }
        return res.toArray(new int[res.size()][]);
    }
}

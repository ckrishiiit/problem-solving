package searching;

public class ShiftedBinarySearch {

    public static int binarySearch(int[] arr, int target, int left, int right) {

        while (left <= right) {

            int mid = left + (right - left) / 2;
            if (arr[mid] == target)
                return mid;

            int leftNum = arr[left];
            int rightNum = arr[right];
            int middleNum = arr[mid];

            if (leftNum <= middleNum) {
                if (target < middleNum && target >= leftNum)
                    right = mid - 1;
                else
                    left = mid + 1;
            } else {
                if (target > middleNum && target <= rightNum)
                    left = mid + 1;
                else
                    right = mid - 1;
            }
        }
        return -1;
    }

    public static void main(String a[]) {
        int[] arr = {45, 61, 71, 72, 73, 0, 1, 21, 33, 37};
        int target = 33;
        System.out.println(binarySearch(arr, 45, 0, arr.length-1));
    }
}

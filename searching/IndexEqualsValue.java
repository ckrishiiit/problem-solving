package searching;

public class IndexEqualsValue {


    public static int indexEqualsValue(int[] arr) {

        int left = 0, right = arr.length - 1;

        while ( left <= right ) {
            int mid = left + (right-left)/2;
            if ( mid == arr[mid]) {
                while ( mid >= 1 && (mid-1) == arr[mid-1]) {
                    mid = mid - 1;
                }
                return mid;
            }
            else if (mid > arr[mid])
                left = mid + 1;
            else
                right = mid - 1;
        }
        return -1;
    }

    public static void main(String[] a) {

        int[] arr = {-1000, 0, 1, 2, 3, 4, 6, 5000, 5001, 5002, 5005};
        System.out.println(indexEqualsValue(arr));
    }
}

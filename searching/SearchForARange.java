package searching;

public class SearchForARange {

    public static int[] searchForRange(int[] arr, int target) {

        int[] finalRange = {-1, -1};
        searchForRange(arr, target, 0, arr.length - 1, finalRange, true);
        searchForRange(arr, target, 0, arr.length - 1, finalRange, false);
        return finalRange;
    }

    private static void searchForRange(int[] arr, int target, int left, int right, int[] finalRange, boolean goLeft) {

        while (left <= right) {
            int mid = left + (right - left)/2;
            if (target < arr[mid])
                right = mid - 1;
            else if ( target > arr[mid])
                left = mid + 1;

            else if (goLeft) {
                if (mid == 0 || arr[mid] != arr[mid-1]) {
                    finalRange[0] = mid;
                    return;
                }else {
                    right = mid - 1;
                }
            }else {
                if (mid == arr.length - 1 || arr[mid] != arr[mid+1]) {
                    finalRange[1] = mid;
                    return;
                }
                else
                    left = mid + 1;
            }
        }
    }


        public static void main(String[] a) {

        int[] arr = {0,1,21,33,45,45,45,45,45,45,61,71,78};
        int[] res = searchForRange(arr, 45);
        System.out.println(res[0] +"," + res[1]);
    }
}

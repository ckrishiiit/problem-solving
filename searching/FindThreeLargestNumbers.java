package searching;

public class FindThreeLargestNumbers {

    private static void updateLargest(int[] arr, int num) {
        if (num > arr[2])
            shiftAndUpdate(arr, num, 2);
        else if (num > arr[1])
            shiftAndUpdate(arr, num, 1);
        else
            shiftAndUpdate(arr, num, 0);
    }

    private static void shiftAndUpdate(int[] arr, int num, int idx) {

        for (int i=0; i<=idx; i++) {
            if ( i == idx)
                arr[i] = num;
            else
                arr[i] = arr[i+1];
        }
    }

    public static int[] findLargest(int[] arr) {

        int[] threeLargest = {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
        for (int num : arr) {
            updateLargest(threeLargest, num);
        }
        return threeLargest;
    }

    public static void main(String[] a) {
        int[] arr = {10,5,9,10,12};
        int[] largest = findLargest(arr);
        for (int no : largest)
            System.out.println(no);
    }
}

package searching;

public class BinarySearch {

    public static int binarySearch(int[] arr, int target) {

        int low = 0, end = arr.length - 1;

        while ( low <= end) {
            int mid = low + (end-low)/2;
            if (arr[mid] == target)
                return mid;
            else if (target < arr[mid])
                end = mid - 1;
            else if (target > arr[mid])
                low = mid + 1;
        }

        return -1;
    }

    public static void main(String[] a) {
        int[] arr = {1,2,3,4,5,6,7,8};
        System.out.println(binarySearch(arr, 2));
    }
}

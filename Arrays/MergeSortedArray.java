package ArraysStrings;

//https://leetcode.com/problems/merge-sorted-array/

public class MergeSortedArray {

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int size = m + n - 1;
        m--; n--;

        /**
         * Iterate over the two arrays and move the larger number
         */
        while ( m >= 0 && n >= 0) {

            if(nums1[m] >= nums2[n]) {
                nums1[size--] = nums1[m--];
            } else {
                nums1[size--] = nums2[n--];
            }
        }

        if (m > 0) {
            while (m >= 0){
                nums1[size--] = nums1[m--];
            }
        }
        else {
            while (n >= 0){
                nums1[size--] = nums2[n--];
            }
        }
    }

    public static void main(String[] a) {

          int[] num1 = {0};
          int[] num2 = {1};
          merge(num1, 0, num2, 1);
    }
}

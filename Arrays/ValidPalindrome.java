package ArraysStrings;

/**
 * https://leetcode.com/problems/valid-palindrome/
 */

public class ValidPalindrome {

    private static boolean isLetterOrDigit(char ch) {

        if ( (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9') ||
                (ch >= 'A' && ch <= 'Z'))
            return true;

        return false;
    }

    public static boolean isPalindrome(String s) {

        int start = 0, end = s.length() - 1;

        if (start == end)
            return true;

        while (start <= end) {

            while ( (start <= end) && s.charAt(start) == ' ' || !isLetterOrDigit(s.charAt(start))) {
                start++;

                if (start > end)
                    return true;
            }

            while (s.charAt(end) == ' ' || !isLetterOrDigit(s.charAt(end))) {
                end--;
            }

            if (Character.toLowerCase(s.charAt(start++)) != Character.toLowerCase(
                    s.charAt(end--)))
                return false;

        }
        return true;
    }


    public static void main(String[] a) {

        System.out.println(isPalindrome("Abc c ba"));
    }

}

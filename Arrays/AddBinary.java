package ArraysStrings;

public class AddBinary {

    public static String addBinary(String a, String b) {
        int aLen = a.length();
        int bLen = b.length();

        StringBuilder sb = new StringBuilder();
        int carry = 0, i = aLen-1, j = bLen-1;
        int sum = 0;

        while ( i >=0 || j >= 0) {
            sum = carry;
            if ( i >= 0)
                sum += a.charAt(i--) - '0';

            if ( j >= 0)
                sum += b.charAt(j--) - '0';

            sb.append(sum % 2);
            carry = sum / 2;
        }

        if (carry == 1)
            sb.append("1");

        return sb.reverse().toString();
    }

    public static void main(String[] a) {
        System.out.println(addBinary("1010","1011"));
    }
}

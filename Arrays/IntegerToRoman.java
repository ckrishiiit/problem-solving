package ArraysStrings;

/**
 * https://leetcode.com/problems/integer-to-roman/
 */

public class IntegerToRoman {

    public static String intToRoman(int num) {

        int[] arabics = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

        String[] romans = {"M", "CM", "D", "CD", "C", "XC", "L", "XL","X", "IX", "V", "IV","I"};

        StringBuilder sb = new StringBuilder();
        for (int i=0; i < arabics.length; i++) {

            while (num - arabics[i] >= 0) {
                sb.append(romans[i]);
                num = num - arabics[i];
            }
        }
        return sb.toString();
    }

    public static void main(String[] a) {
      System.out.println(intToRoman(399));
    }
}

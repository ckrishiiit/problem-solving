package ArraysStrings;

/**
 * https://leetcode.com/problems/string-to-integer-atoi/
 */


public class StringToInteger {

    public static int myAtoi(String s) {

         if (s == null || s.length() == 0)
             return 0;

         // trim string to remove white spaces
         s = s.trim();

         if (s.isEmpty())
             return 0;
         boolean isNegative = false;
         int index = 0;

         if (s.charAt(index) == '-')
             isNegative = true;
         // Move index 1 step if sign is there
        double res = 0;
        if (s.charAt(index) == '+' || s.charAt(index) == '-')
            index++;

        for (int i=index; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch < '0' || ch > '9')
                break;

            int digitValue = ch - '0';
            res = res * 10 + digitValue;
        }

        if (isNegative)
            res = res * -1;

        if (res > Integer.MAX_VALUE)
            return Integer.MAX_VALUE;
        else if (res < Integer.MIN_VALUE)
            return Integer.MIN_VALUE;

        return (int) res;
    }

    public static void main(String[] a) {
      System.out.println(myAtoi("ww  -123  "));
    }
}

package Strings;

public class StringToInt {

    public static int stringToInt(String s) {

        return (s.charAt(0) == '-' ? -1 : 1) *
                s.substring( (s.charAt(0) == '-' || s.charAt(0) == '+') ? 1 : 0)
                 .chars()
                 .reduce(0, (runningSum,c) -> runningSum * 10 + c - '0');
    }

    public static void main(String[] a){
        System.out.println(stringToInt("123"));
    }
}

package ArraysStrings;

/**
 * https://leetcode.com/problems/implement-strstr/
 */
public class ImplementStrStr {


    public static int strStr(String haystack, String needle) {

        if (needle == null || needle.length() == 0)
            return 0;

        if (needle.length() > haystack.length())
            return -1;

        int j = 0;

        for (int i=0; i < haystack.length() - needle.length() + 1; i++) {

            for (j=0; j < needle.length(); j++) {
                if (haystack.charAt(i+j) != needle.charAt(j))
                    break;
            }
            if (j == needle.length())
                return i;
        }
        return -1;
    }

    public static void main(String[] a) {

        System.out.print(strStr("hello","ll"));
    }
}

package Strings;

public class ReverseWordsInAString {

    private static void reverseStr(char[] arr,int start, int end) {
        while (start <= end) {
            char temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;

            start++;
            end--;
        }
    }

    public static String reverseWords(String s) {

         s = s.trim();
         char[] arr = s.toCharArray();
         reverseStr(arr, 0 , arr.length - 1);

         int writeIndex = 0, i = 0;
         while (i < arr.length ){
             if (arr[i] == ' ') {
                 arr[writeIndex++] = arr[i];
                 while ( i < arr.length && arr[i] == ' '){
                     i = i+1;
                 }

             } else {
                 arr[writeIndex++] = arr[i++];
             }
         }


         int index = 0, start = 0;
         while ( index < writeIndex ) {
             if (arr[index] == ' ') {
                 reverseStr(arr, start, index - 1);
                 start = index + 1;
             }
             index++;
         }

        reverseStr(arr, start, writeIndex - 1 );
         return new String(arr).substring(0, writeIndex);
    }

    public static void main(String[] a){
        String str = "a good   example";
        System.out.println(reverseWords(str));
    }
}

package ArraysStrings;

//https://leetcode.com/problems/group-anagrams/

import java.util.*;

public class GroupAnagrams {

    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();

        for (String word: strs) {

            char[] arr = word.toCharArray();
            Arrays.sort(arr);
            String sortedWord = new String(arr);

            if (map.containsKey(sortedWord)) {
                map.get(sortedWord).add(word);

            } else {
                //map.put(sortedWord, new ArrayList<>(Arrays.asList(word)));

                List<String> list = new ArrayList<>();
                list.add(word);
                map.put(sortedWord, list);
            }
        }
        return new ArrayList<>(map.values());
    }

    public static void main(String[] a) {
        String[] str = {"eat","tea","tan","ate","nat","bat"};
        List<List<String>> list = groupAnagrams(str);

        for(List<String> res: list) {
            for (String s: res) {
                System.out.print(s +" ");
            }
            System.out.println();
        }
    }
}

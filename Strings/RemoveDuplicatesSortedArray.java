package ArraysStrings;

/**
 * https://leetcode.com/problems/remove-duplicates-from-sorted-array/
 */
public class RemoveDuplicatesSortedArray {

    public static int removeDuplicates(int[] nums) {
        int len = nums.length;
        int writeIndex = 1;
        for (int i=1; i < len; i++) {
            if (nums[writeIndex-1] != nums[i]) {
                nums[writeIndex++] = nums[i];
            }
        }
        return writeIndex;
    }

    public static void main(String[] a) {
        int[] arr = {1,1,2};
        System.out.print(removeDuplicates(arr));
    }
}



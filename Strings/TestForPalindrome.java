package Strings;
/*
   Ignore alphanumeric in the string and check whether it's palindrome or not
 */

public class TestForPalindrome {

    public static boolean isPalindrome(String s) {

        int left = 0, right = s.length() - 1;

        while (left < right) {

            while ( left < right && !Character.isLetterOrDigit(s.charAt(left)))
                ++left;

            while ( left < right && !Character.isLetterOrDigit(s.charAt(right)))
                --right;

            if (Character.toLowerCase(s.charAt(left++)) !=
                    Character.toLowerCase(s.charAt(right--))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] a) {

        System.out.println(isPalindrome("A b c c b a"));
    }
}

package Strings;
import java.util.*;

/**
 * Given char array replace 'a' with 'd' and remove character 'b'.
 * */

//Time - O(n)
//Space - O(1)

public class ReplaceAndRemove {

    public static int replaceAndRemove(char[] s, int size) {

         int aCount = 0, writeIndex = 0;

         for (int i=0; i < size; i++) {
              if (s[i] != 'b') {
                  s[writeIndex++] = s[i];
              }
              if (s[i] == 'a') {
                  aCount++;
              }
         }

         int currIndex = writeIndex - 1;
         writeIndex = writeIndex + aCount - 1;
         int finalSize = writeIndex + 1;

         while (currIndex >= 0) {
             if (s[currIndex] == 'a') {
                 s[writeIndex--] = 'd';
                 s[writeIndex--] = 'd';
             }else {
                 s[writeIndex--] = s[currIndex];
             }
             currIndex--;
         }

         for (char c: s) {
             System.out.print(c +" ");
         }
         return finalSize;
    }

    public static void main(String[] a) {
        char[] s = {'a','b'};
        replaceAndRemove(s,2);

    }
}

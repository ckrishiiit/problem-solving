package ArraysStrings;

/**
 * https://leetcode.com/problems/valid-palindrome-ii/
 *
 * Given a string s, return true if the s can be palindrome after
 * deleting at most one character from it.
 */

public class ValidPalindromeII {

    private static boolean isPalindrome(String s, int start, int end) {

        while (start <= end) {
            if (s.charAt(start++) != s.charAt(end--))
                return false;
        }
        return true;
    }

    public static boolean validPalindrome(String s) {

        int start = 0, end = s.length() - 1;

        while (start <= end && ( s.charAt(start) == s.charAt(end) )) {
            start++;
            end--;
        }
        return isPalindrome(s, start+1, end) || isPalindrome(s, start, end-1);
    }

    public static void main(String[] a) {

        System.out.println(validPalindrome("abc"));
    }
}

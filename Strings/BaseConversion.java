package Strings;

/**
 * Write a function which takes string & two integers (b1 , b2) as input.
 * String is in b1 base convert it into b2 base.
 */

public class BaseConversion {

    public static String convertBase(String numAsString, int b1, int b2) {
        boolean isNegative = numAsString.startsWith("-");
        int numAsInt =
                numAsString.substring(isNegative ? 1 : 0)
                           .chars()
                           .reduce(0, (x,c) -> x * b1 +
                                   (Character.isDigit(c) ? c - '0' : c - 'A' + 10));

        return (isNegative ? "-" :"") +
                (numAsInt == 0? "" : constructBaseFrom(numAsInt, b2));
    }

    private static String constructBaseFrom(int numAsInt, int base) {
        return numAsInt == 0
                ? ""
                : constructBaseFrom(numAsInt/base, base) +
                (char) (numAsInt % base >= 10 ? 'A' + numAsInt % base - 10
                                               : '0' + numAsInt % base);
    }
}

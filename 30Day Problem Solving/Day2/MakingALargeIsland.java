package Day2;
import java.util.*;

/**
 * https://leetcode.com/problems/making-a-large-island/
 */
public class MakingALargeIsland {
    private static int[][] DIRECTIONS =  {{0,1}, {1,0}, {0,-1}, {-1,0}};
    int maxArea = 0;
    public void bfs(int[][] grid, int rows, int cols, int currRow, int currCol,
                    Map<Integer, Integer> map, int isLandId) {

        int currMaxArea = 0;
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{currRow, currCol});
        grid[currRow][currCol] = isLandId;
        map.put(isLandId, map.get(isLandId)+1);

        while (!queue.isEmpty()) {
            currMaxArea++;
            int[] box = queue.poll();
            int row = box[0];
            int col = box[1];

            for (int[] dir : DIRECTIONS) {
                int newRow = dir[0] + row;
                int newCol = dir[1] + col;

                if (newRow < 0 || newCol < 0 || newRow >= rows || newCol >= cols
                        || grid[newRow][newCol] == 0 || grid[newRow][newCol] >= 2) {
                    continue;
                }

                grid[newRow][newCol] = isLandId;
                map.put(isLandId, map.get(isLandId)+1);
                queue.add(new int[]{newRow, newCol});
            }
        }
        maxArea = Math.max(maxArea, currMaxArea);
    }

    public int largestIsland(int[][] grid) {

        if (grid == null || grid.length == 0)
            return 0;

        int rows = grid.length;
        int cols = grid[0].length;

        int isLandId = 2;

        Map<Integer, Integer> map = new HashMap<>();

        for (int i=0; i < rows; i++) {
            for (int j=0; j < cols; j++) {
                if (grid[i][j] == 1) {

                    map.put(isLandId, 0);
                    bfs(grid, rows, cols, i, j, map, isLandId);
                    isLandId++;
                }
            }
        }
        calculateMaxArea(grid, rows, cols,map);
        return maxArea;
    }

    public void calculateMaxArea(int[][] grid, int rows, int cols,Map<Integer, Integer> map) {

        for (int i=0; i < rows; i++) {
            for (int j=0; j < cols; j++) {

                if (grid[i][j] == 0) {

                    Set<Integer> set = new HashSet<>();
                    for (int[] dir : DIRECTIONS) {
                        int newRow = dir[0] + i;
                        int newCol = dir[1] + j;

                        if (newRow < 0 || newCol < 0 || newRow >= rows || newCol >= cols) {
                            continue;
                        }
                        int index = grid[newRow][newCol];
                        if (map.containsKey(index)) {
                            set.add(index);
                        }
                    }

                    int currArea = 0;
                    for (Integer id : set) {
                        currArea += map.get(id);
                    }
                    maxArea = Math.max(currArea + 1, maxArea);
                }
            }
        }
    }
}

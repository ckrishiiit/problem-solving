package Day2;
import java.util.*;

/**
 * https://leetcode.com/problems/nested-list-weight-sum/
 */
interface NestedInteger{

    public boolean isInteger();
    public int getInteger();
}

public class NestedListWeightSum {

    public int depthSum(List<NestedInteger> nestedList) {
        return dfs(nestedList, 1);
    }

    private int dfs(List<NestedInteger> nestedList, int depth) {
        int total = 0;
        for (NestedInteger list: nestedList) {
            if (list.isInteger()){
                total += list.getInteger() * depth;
            }else
                total += dfs(nestedList, depth + 1);
        }
        return total;
    }
}

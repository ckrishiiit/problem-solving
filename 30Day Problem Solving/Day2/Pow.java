package Day2;

public class Pow {
    public double myPow(double x, int n) {
        long N = n;
        if (N < 0) {
            x = 1 / x;
            N = -N;
        }
        double ans = 1;
        double current_product = x;
        for (long i = N; i > 0; i /= 2) {
            if ((i % 2) == 1) {
                ans = ans * current_product;
            }
            current_product = current_product * current_product;
        }
        return ans;
    }
    public static void main(String[] a){
        Pow p = new Pow();
        double val = p.myPow(2, -2147483648);
        System.out.println(val);
    }
}

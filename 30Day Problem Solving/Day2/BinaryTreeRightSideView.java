package Day2;
import java.util.*;

/**
 * https://leetcode.com/problems/binary-tree-right-side-view/submissions/
 */
public class BinaryTreeRightSideView {

    public List<Integer> rightSideView(TreeNode root) {

        List<Integer> res = new ArrayList<>();
        if (root == null)
            return res;

        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()) {

            int levelSize = queue.size();
            for (int i=0; i < levelSize; i++) {

                root = queue.poll();
                if (i == levelSize-1) {
                    res.add(root.val);
                }

                if (root.left != null)
                    queue.add(root.left);

                if (root.right != null)
                    queue.add(root.right);
            }
        }

        return res;
    }
}

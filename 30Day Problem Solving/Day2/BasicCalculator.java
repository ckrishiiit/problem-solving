package Day2;
import java.util.*;

/**
 * https://leetcode.com/problems/basic-calculator/
 */
public class BasicCalculator {

    private int evalauteExp(Deque<Object> stack){

        if (!(stack.peek() instanceof Integer))
            stack.push(0);

        int res = (int) stack.pop();
        while (!stack.isEmpty() && !((char) stack.peek() == ')')) {

            char sign = (char) stack.pop();
            if (sign == '+') {
                res += (int) stack.pop();
            }

            if (sign == '-') {
                res -= (int) stack.pop();
            }
        }
        return res;
    }

    public int calculate(String s) {

        if (s == null || s.length() == 0)
            return 0;

        Deque<Object> stack = new ArrayDeque<>();
        int operand = 0;
        int index = 0;

        for (int i=s.length() - 1; i >=0; i--){
            char ch = s.charAt(i);
            if (Character.isDigit(ch)) {
                operand = (int) (Math.pow(10,index) * (ch-'0')) + operand;
                index++;
            }

           else if (ch != ' '){
                if (index != 0) {
                    stack.addFirst(operand);
                    index = 0;
                    operand = 0;
                }

                if (ch == '(') {
                    int res = evalauteExp(stack);
                    stack.pop();
                    stack.push(res);
                }else {
                    stack.addFirst(ch);
                }
            }
        }
        if (index != 0)
            stack.push(operand);

        return evalauteExp(stack);
    }

    public static void main(String[] a) {
        String s = "-11";
        BasicCalculator obj = new BasicCalculator();
        System.out.println(obj.calculate(s));
    }


}

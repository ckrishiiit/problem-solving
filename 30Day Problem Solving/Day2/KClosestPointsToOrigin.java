package Day2;

/**
 * https://leetcode.com/problems/k-closest-points-to-origin/
 */
public class KClosestPointsToOrigin {

    private int distance(int[] point) {
        return point[0] * point[0] + point[1] * point[1];
    }

    private void swap(int[][] points,int left, int right) {
        int[] temp = points[left];
        points[left] = points[right];
        points[right] = temp;
    }

    private void helper(int[][] points, int position,int start, int end) {

        while (true) {
            int pivot = start;
            int left  = start + 1;
            int right = end;

            int pivotDistance = distance(points[pivot]);

            while (left <= right) {
                int leftPointDistance = distance(points[left]);
                int rightPointDistance = distance(points[right]);


                if (leftPointDistance > pivotDistance && rightPointDistance <
                        pivotDistance) {
                    swap(points, left, right);
                }

                if (leftPointDistance <= pivotDistance)
                    left++;

                if (rightPointDistance >= pivotDistance)
                    right--;
            }
            swap(points, right, pivot);
            if (right == position)
                return;

            if (right > position)
                end = right - 1;
            else
                start = right + 1;
        }
    }

    public int[][] kClosest(int[][] points, int k) {

        int position = k-1;
        helper(points, position, 0, points.length-1);

        int[][] res = new int[k][2];
        for (int i=0; i <= position; i++) {
            res[i] = points[i];
        }
        return res;
    }
}

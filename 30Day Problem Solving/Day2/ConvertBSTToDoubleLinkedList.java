package Day2;

/**
 * https://leetcode.com/problems/convert-binary-search-tree-to-sorted-doubly-linked-list/
 */
class Node{
    int val;
    Node left;
    Node right;

    public Node(int val){
        this.val = val;
        this.left = null;
        this.right = null;
    }
}

public class ConvertBSTToDoubleLinkedList {

    Node first = null;
    Node last = null;

    private void helper(Node node) {

        if (node != null) {
            helper(node.left);
            if (first == null) {
                first = node;
            }else {
                last.right = node;
                node.left = last;
            }
            last = node;
            helper(node.right);
        }
    }

    public Node treeToDoublyList(Node root) {

        if (root == null)
            return null;

        helper(root);

        first.left = last;
        last.right = first;

        return first;
    }
}

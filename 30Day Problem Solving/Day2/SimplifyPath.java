package Day2;

import java.util.*;

/**
 * https://leetcode.com/problems/simplify-path/
 */
public class SimplifyPath {

    public String simplifyPath(String path) {
         if (path == null || path.isEmpty())
             return "";

         String[] directories = path.split("/");
         Deque<String> stack = new ArrayDeque<>();

         for (String dir : directories) {
             if (".".equals(dir) || dir.isEmpty())
                 continue;

             if ("..".equals(dir)){
                 if (!stack.isEmpty())
                     stack.poll();
             }
             else
                 stack.addFirst(dir);
         }

         StringBuilder sb = new StringBuilder();
         while(!stack.isEmpty()) {
             sb.append("/").append(stack.removeLast());
         }
         return sb.length() == 0? "/" : sb.toString();
    }
    public static void main(String[] a) {
        String s = "/../";
        SimplifyPath obj = new SimplifyPath();
        System.out.println(obj.simplifyPath(s));
    }
}

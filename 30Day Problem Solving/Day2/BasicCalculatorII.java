package Day2;
import java.util.*;

public class BasicCalculatorII {
    public int calculate(String s) {

        /*if (s == null || s.length() == 0)
            return 0;

        Stack<Integer> stack = new Stack<>();
        int currNum = 0;
        char op = '+';

        for (int i=0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (Character.isDigit(ch)) {
                currNum = (currNum * 10) + ch - '0';
            }

            if ( (!Character.isDigit(ch) && !Character.isWhitespace(ch))
                    || i == s.length() - 1) {

                if (op == '+') {
                    stack.push(currNum);
                }

                if (op == '-') {
                    stack.push(-currNum);
                }

                if (ch == '*'){
                    stack.push(stack.pop() * currNum);
                }

                if (ch == '/') {
                    stack.push(stack.pop() / currNum);
                }

                op = ch;
                currNum = 0;
            }
        }

        int res = 0;
        while (!stack.isEmpty()) {
            res += stack.pop();
        }

        return res;*/


            if (s == null || s.length() == 0)
                return 0;

            int res = 0;
            int currNum = 0;
            int prevNum = 0;

            char op = '+';
            for (int i=0; i < s.length(); i++) {
                char ch = s.charAt(i);

                if (Character.isDigit(ch)) {
                    currNum = (currNum * 10) + (ch - '0');
                }

                if ((!Character.isDigit(ch) && !Character.isWhitespace(ch))
                        || i == s.length()-1) {

                    if (op == '+' || op == '-') {
                        res += prevNum;
                        prevNum = (op == '+') ? currNum : -currNum;
                    }

                    if (op == '*') {
                        prevNum = prevNum * currNum;
                    }

                    if (op == '/') {
                        prevNum = prevNum / currNum;
                    }
                    op = ch;
                    currNum = 0;
                }
            }
            return (res + prevNum);
    }

    public static void main(String[] a) {
        BasicCalculatorII obj = new BasicCalculatorII();
        int res = obj.calculate("42");
        System.out.println(res);
    }
}

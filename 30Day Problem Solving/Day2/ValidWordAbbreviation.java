package Day2;

/**
 * https://leetcode.com/problems/valid-word-abbreviation/
 * Time O(M + N)
 * Space: O(1)
 */
public class ValidWordAbbreviation {

    public boolean validWordAbbreviation(String word, String abbr) {

        char[] wordArr = word.toCharArray();
        char[] abbrArr = abbr.toCharArray();

        int i = 0, j = 0;

        while (i < wordArr.length && j < abbrArr.length) {
            char abbrCh = abbrArr[j];
            if (Character.isDigit(abbrCh)) {

                int indexToMove = 0;

                if (abbrCh == '0') {
                    return false;
                }

                while ( j < abbrArr.length && Character.isDigit(abbrArr[j])) {
                    indexToMove = (indexToMove * 10) + abbrArr[j] - '0';
                    j++;
                }
                i += indexToMove;

            }else{

                if (wordArr[i] != abbrArr[j])
                    return false;
                i++;
                j++;
            }
        }
        return i == wordArr.length && j == abbrArr.length;
    }
}

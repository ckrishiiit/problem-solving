package Day1;

/**
 * https://leetcode.com/problems/valid-palindrome-iii/
 */
public class ValidPalindromeIII {

    private int isValidPalindrome(String s, int left, int right, Integer[][] dp) {

        if (left == right)
            return 0;

        if (left == right - 1)
            return s.charAt(left) == s.charAt(right) ? 0 : 1;

        if (dp[left][right] != null)
            return dp[left][right];

        if (s.charAt(left) == s.charAt(right))
            return dp[left][right] = isValidPalindrome(s, left+1, right-1, dp);

        dp[left][right] = 1 + Math.min(isValidPalindrome(s, left+1, right, dp)
        , isValidPalindrome(s, left, right-1, dp));

        return dp[left][right];
    }

    public boolean isPalindrome(String s, int k) {

        if (s == null || s.length() == 0)
            return true;

        Integer[][] dp = new Integer[s.length()][s.length()];
        return isValidPalindrome(s, 0, s.length()-1, dp) <= k;
    }
    public static void main(String[] a) {
        String s = "agecddba";
        ValidPalindromeIII obj = new ValidPalindromeIII();
        boolean valid = obj.isPalindrome(s, 4);
        System.out.println(valid);
    }
}

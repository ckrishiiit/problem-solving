package Day1;

/**
 * https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
 */

public class MinimumRemoveToMakeValidParentheses {

    public String minRemoveToMakeValid(String s) {

        if ( s == null || s.length() == 0)
            return "";

        int open = 0;
        int balance = 0;

        StringBuilder sb = new StringBuilder();

        for (int i=0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if ( ch == '('){
                open++;
                balance++;
            }else if (ch == ')'){
                if (balance <= 0)
                    continue;

                balance--;
            }
            sb.append(ch);
        }

        //need to remove extra open spaces
        StringBuilder res = new StringBuilder();
        int openToKeep = open - balance;

        for (int i=0; i < sb.length(); i++) {
            char ch = sb.charAt(i);
            if (ch == '(') {
                if (openToKeep-- <= 0)
                    continue;
            }
            res.append(ch);
        }

        return res.toString();
    }
    public static void main(String[] a) {
        MinimumRemoveToMakeValidParentheses obj = new MinimumRemoveToMakeValidParentheses();
        String s = "ab(cd)(e(f";
        String minStr = obj.minRemoveToMakeValid(s);
        System.out.println(minStr);
    }
}

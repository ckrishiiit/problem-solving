package Day1;

/**
 * https://leetcode.com/problems/valid-palindrome-ii/
 */
public class ValidPalindromeII {

    private boolean removeCharAndCheck(String s, int left, int right) {

        while (left < right) {
            if (s.charAt(left) != s.charAt(right))
                return false;

            left++;
            right--;
        }
        return true;
    }
    public boolean validPalindrome(String s) {

        if (s == null || "".equals(s)) {
            return true;
        }

        int left = 0, right = s.length() - 1;
        while (left < right) {

            if (s.charAt(left) != s.charAt(right)) {
                return removeCharAndCheck(s, left+1, right) ||removeCharAndCheck(s, left, right-1);
            }

            left++;
            right--;
        }
        return true;
    }
}

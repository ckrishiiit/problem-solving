package EPI.Graphs;
import java.util.*;

/***
 * Using DFS Time complexity : O(V+E)
 * Initiate DFS from source. In the DFS path if able to find destination coordinate
 * then return true.
 */
class Coordinate{

    int x;
    int y;

    public Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Coordinate that = (Coordinate)o;
        if (x != that.x || y != that.y)
            return false;

        return true;
    }
}

public class SearchMaze {

    public enum Color { WHITE, BLACK}
    private static int[][] DIRECTIONS =  {{1,0}, {0,1}, {0,-1}, {-1,0}};

    public static List<Coordinate> searchMaze(List<List<Color>> maze, Coordinate s, Coordinate e) {

        List<Coordinate> path = new ArrayList<>();
        helper(maze, s, e, path);
        return path;
    }

    private static boolean helper(List<List<Color>> maze, Coordinate curr, Coordinate e, List<Coordinate> path) {

        if (curr.x < 0 || curr.x >= maze.size() || curr.y <  0 || curr.y >= maze.get(curr.x).size()
                || maze.get(curr.x).get(curr.y) != Color.WHITE) {
            return false;
        }

        path.add(curr);
        maze.get(curr.x).set(curr.y, Color.BLACK);
        if (curr.equals(e))
            return true;

        for (int[] dir : DIRECTIONS) {
            int newRow = dir[0] + curr.x;
            int newCol = dir[1] + curr.y;

            if (helper(maze, new Coordinate(newRow, newCol), e, path))
                return true;
        }
        path.remove(path.size()-1);
        return false;
    }
}
